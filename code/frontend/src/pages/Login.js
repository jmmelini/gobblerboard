import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "@mui/material";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import { Link as RouterLink } from "react-router-dom";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [user, setUser] = useState(null);

  const navigate = useNavigate();

  const onButtonClick = async () => {
    localStorage.clear();
    setUsernameError("");
    setPasswordError("");

    if (!username || !password) {
      if (!username) setUsernameError("Please enter your username");
      if (!password) setPasswordError("Please enter a password");
      return;
    }

    try {
      const response = await fetch("http://localhost:5000/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ username, password }),
      });

      const data = await response.json();
      if (response.ok) {
        setUser({ user_id: data.user_id, username: data.username }); // Store user data in state
        navigate("/home");
        localStorage.setItem(
          "user",
          JSON.stringify({
            user_id: data.user_id,
            username: data.username,
          })
        );
      } else {
        const error = data.message;
        if (response.status === 401) setPasswordError(error);
        else if (response.status === 404) setUsernameError(error);
        else alert("Login failed: " + error);
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Network error: Unable to connect to the server.");
    }
  };
  return (
    <div className={"mainContainer"}>
      <div className={"titleContainer"}>
        <div>Login</div>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          type="text"
          value={username}
          onChange={(ev) => setUsername(ev.target.value)}
          placeholder="Enter your username here"
          className={"inputBox"}
        />
        <label className="errorLabel">{usernameError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          type="password"
          value={password}
          onChange={(ev) => setPassword(ev.target.value)}
          placeholder="Enter your password here"
          className={"inputBox"}
        />
        <label className="errorLabel">{passwordError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <Button variant="outlined" onClick={onButtonClick}>
          Login
        </Button>
      </div>
      <Grid container justifyContent="center" sx={{ mt: 2 }}>
        <Grid item>
          <Link href="#" variant="body2" component={RouterLink} to="/signup">
            {"Don't have an account? Sign Up"}
          </Link>
        </Grid>
      </Grid>
    </div>
  );
};

export default Login;
