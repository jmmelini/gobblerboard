import { Button } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link as RouterLink } from "react-router-dom";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";

const Login = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const navigate = useNavigate();

  const onButtonClick = async () => {
    // Set initial error values to empty
    setUsernameError("");
    setPasswordError("");

    // Check if the user has entered both fields correctly
    if (username === "") {
      setUsernameError("Please enter your username");
      return;
    }

    if (password === "") {
      setPasswordError("Please enter a password");
      return;
    }

    if (password.length < 8) {
      setPasswordError("The password must be 8 characters or longer");
      return;
    }

    const data = { username, password };

    // Send POST request to /signup endpoint
    try {
      const response = await fetch("http://localhost:5000/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      const responseData = await response.json(); // Parse the JSON from the response

      if (response.ok) {
        // Store the user_id and username in localStorage
        localStorage.setItem(
          "user",
          JSON.stringify({
            user_id: responseData.user_id,
            username: responseData.username,
          })
        );
        navigate("/home");
      } else {
        // Handle server errors or unsuccessful signup
        alert("Signup failed: " + responseData.message);
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Network error: Unable to connect to the server.");
    }
  };

  return (
    <div className={"mainContainer"}>
      <div className={"titleContainer"}>
        <div>Signup</div>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          value={username}
          placeholder="Enter your username here"
          onChange={(ev) => setUsername(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{usernameError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          type="password"
          value={password}
          placeholder="Enter your password here"
          onChange={(ev) => setPassword(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{passwordError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <Button variant="outlined" onClick={onButtonClick}>
          Signup
        </Button>
      </div>
      <Grid container justifyContent="center" sx={{ mt: 2 }}>
        <Grid item>
          <Link href="#" variant="body2" component={RouterLink} to="/">
            {"Already have an account? Login"}
          </Link>
        </Grid>
      </Grid>
    </div>
  );
};

export default Login;
