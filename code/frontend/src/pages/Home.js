import ButtonAppBar from "../components/ButtonAppBar";
import Post from "../components/Post";
import React, { useState, useEffect } from "react";
import { Button } from "@mui/material";
import CreatePostDialog from "../components/CreatePostDialog";
import ChangePasswordDialog from "../components/ChangePasswordDialog";
import { Link as RouterLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const [open, setOpen] = useState(false);
  const [openPassword, setOpenPassword] = useState(false);
  const [posts, setPosts] = useState([]);
  const [isAdmin, setIsAdmin] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    fetchPosts();
    fetchUserModLevel();
  }, []);

  const fetchUserModLevel = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
      console.log("No user logged in");
      return;
    }

    try {
      const response = await fetch(
        `http://localhost:5000/getmodlevel/${user.user_id}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
        }
      );

      const data = await response.json();
      if (response.ok) {
        setIsAdmin(data.modLevel === 1); // 1 signifies admin
      } else {
        console.error("Failed to fetch mod level:", data.message);
      }
    } catch (error) {
      console.error("Network error while fetching mod level:", error);
    }
  };

  const fetchPosts = async () => {
    try {
      const response = await fetch("http://localhost:5000/posts");
      const data = await response.json();
      if (response.ok) {
        console.log(data);
        setPosts(data);
      } else {
        console.error("Failed to fetch posts:", data.message);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleOpenPassword = () => {
    setOpenPassword(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleClosePassword = () => {
    setOpenPassword(false);
  };

  const handleCreate = async (content) => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
      alert("You need to log in to create a post.");
      return;
    }

    const newPost = {
      Post_ID: posts.length + 1,
      User_ID: user.user_id,
      Content: content,
    };

    try {
      const response = await fetch("http://localhost:5000/createpost", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newPost),
      });

      const postData = await response.json();
      if (response.ok) {
        fetchPosts();
      } else {
        alert("Failed to create post: " + postData.message);
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Network error: Unable to connect to the server.");
    }
  };

  function handleCreatePassword() {}

  const handleDelete = async (id) => {
    if (!window.confirm("Are you sure you want to delete this post?")) {
      return;
    }

    try {
      const response = await fetch(`http://localhost:5000/deletepost/${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const result = await response.json();
      if (response.ok) {
        fetchPosts();
        console.log("Delete successful:", result.message);
      } else {
        console.error("Failed to delete post:", result.message);
        alert("Failed to delete post: " + result.message);
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Network error: Unable to connect to the server.");
    }
  };

  function goToGenerateReports() {
    navigate("/reports");
  }

  return (
    <div>
      <ButtonAppBar
        onGenerateReports={goToGenerateReports}
        onCreatePost={handleOpen}
        isAdmin={isAdmin}
        onChangePassword={handleOpenPassword}
      />
      <CreatePostDialog
        open={open}
        handleClose={handleClose}
        handleCreate={handleCreate}
      />
      <ChangePasswordDialog
        open={openPassword}
        handleClose={handleClosePassword}
        handleCreate={handleCreatePassword}
      />
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
          alignItems: "start",
        }}
      >
        {posts.map((post) => (
          <Post
            key={post.Post_ID || post.id}
            username={post.Username || post.username}
            content={post.Content || post.content}
            deletePost={handleDelete}
            id={post.Post_ID || post.id}
            isAdmin={
              isAdmin ||
              JSON.parse(localStorage.getItem("user")).username ===
                post.Username
            }
          />
        ))}
      </div>
    </div>
  );
}
