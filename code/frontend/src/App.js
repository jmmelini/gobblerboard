import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/Login.js";
import Home from "./pages/Home.js";
import "./App.css";
import BarChartSVG from "./pages/BarChartSVG.js";
import { useEffect, useState } from "react";
import Signup from "./pages/Signup.js";

export default function App() {
  const [loggedIn, setLoggedIn] = useState(false);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login setLoggedIn={setLoggedIn} />} />
          <Route path="/home" element={<Home />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/reports" element={<BarChartSVG />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
