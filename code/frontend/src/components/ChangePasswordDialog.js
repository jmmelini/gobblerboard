import React, { useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
} from "@mui/material";

function ChangePasswordDialog({ open, handleClose, userId }) {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");

  const handleOldPasswordChange = (event) => {
    setOldPassword(event.target.value);
  };

  const handleNewPasswordChange = (event) => {
    setNewPassword(event.target.value);
  };

  const handleSubmit = async () => {
    if (!oldPassword || !newPassword) {
      alert("Please fill in both password fields.");
      return;
    }

    try {
      const response = await fetch(
        `http://localhost:5000/changePassword/${
          JSON.parse(localStorage.getItem("user")).user_id
        }`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ oldPassword, newPassword }),
        }
      );

      const data = await response.json();
      if (response.ok) {
        alert("Password changed successfully!");
        handleClose();
      } else {
        alert(data.message); // Show error message from server
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Failed to change password due to network error.");
    }
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Change Password</DialogTitle>
      <DialogContent>
        <TextField
          margin="dense"
          id="oldPassword"
          label="Old Password"
          type="password" // Change type to password
          fullWidth
          value={oldPassword}
          onChange={handleOldPasswordChange}
        />
        <TextField
          margin="dense"
          id="newPassword"
          label="New Password"
          type="password" // Change type to password
          fullWidth
          value={newPassword}
          onChange={handleNewPasswordChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit}>Change Password</Button>
      </DialogActions>
    </Dialog>
  );
}

export default ChangePasswordDialog;
