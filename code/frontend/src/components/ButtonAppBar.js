import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import { useNavigate } from "react-router-dom";

export default function ButtonAppBar(props) {
  const navigate = useNavigate();

  function onLogout() {
    navigate("/");
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <div style={{ flex: 1, display: "flex" }}>
            {props.isAdmin && (
              <Button
                onClick={props.onGenerateReports}
                color="inherit"
                variant="outlined"
                sx={{ mr: 2 }}
              >
                Generate Reports
              </Button>
            )}
            <Button
              color="inherit"
              variant="outlined"
              onClick={props.onCreatePost}
            >
              Create Post
              <AddIcon color="white" style={{ marginLeft: "7px" }} />
            </Button>
          </div>
          <Typography
            variant="h4"
            component="div"
            sx={{ flex: 1, textAlign: "center" }}
          >
            Gobblerboard
          </Typography>
          <div style={{ flex: 1, display: "flex", justifyContent: "flex-end" }}>
            <Button
              color="inherit"
              variant="outlined"
              sx={{ mr: 2 }}
              onClick={props.onChangePassword}
            >
              Change Password
            </Button>
            <Button color="inherit" variant="outlined" onClick={onLogout}>
              Logout
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
