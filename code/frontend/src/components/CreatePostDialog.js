import React, { useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
} from "@mui/material";

function CreatePostDialog({ open, handleClose, handleCreate }) {
  const [content, setContent] = useState("");

  const handleContentChange = (event) => {
    setContent(event.target.value);
  };

  const handleSubmit = () => {
    handleCreate(content);
    handleClose(); // Close the dialog on submit
    setContent(""); // Reset content field
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Create New Post</DialogTitle>
      <DialogContent>
        <TextField
          margin="dense"
          id="content"
          label="Content"
          type="text"
          fullWidth
          multiline
          rows={4}
          variant="outlined"
          value={content}
          onChange={handleContentChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit}>Create</Button>
      </DialogActions>
    </Dialog>
  );
}

export default CreatePostDialog;
