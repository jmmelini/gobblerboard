import React, { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import ReportProblemIcon from "@mui/icons-material/ReportProblem";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import ThumbDownIcon from "@mui/icons-material/ThumbDown";

import {
  CardActions,
  Button,
  TextField,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

export default function Post({ username, content, deletePost, id, isAdmin }) {
  const [comments, writeComment] = useState([]);
  const [text, makeText] = useState("");
  const [isReported, setIsReported] = useState(false);
  const [likesNum, setLikesNum] = useState(0);
  const [dislikesNum, setDislikesNum] = useState(0);

  // Function to fetch comments from the backend
  const fetchComments = async () => {
    try {
      if (!id) {
        console.error("Post ID is missing, unable to fetch comments.");
        return;
      }

      const response = await fetch(
        `http://localhost:5000/comments?post_id=${id}`
      );
      const data = await response.json();
      console.log("Comments fetched:", data);
      if (response.ok) {
        writeComment(
          data.map(
            (comment) => "[" + comment.Username + "] " + "- " + comment.Content
          )
        );
        console.log("Comments fetched:", data);
      } else {
        console.error("Failed to fetch comments:", data.message);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  const fetchVotes = async () => {
    try {
      const response = await fetch(`http://localhost:5000/getvotes/${id}`);
      const data = await response.json();
      if (response.ok) {
        setLikesNum(data.likes);
        setDislikesNum(data.dislikes);
      } else {
        console.error("Failed to fetch votes:", data.message);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  const checkIfReported = async () => {
    try {
      const response = await fetch(`http://localhost:5000/checkreport/${id}`);
      const data = await response.json();
      if (response.ok) {
        setIsReported(data.reported);
      } else {
        console.error("Failed to check if reported:", data.message);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };

  useEffect(() => {
    fetchComments();
    checkIfReported();
    fetchVotes();
  }, [id]);

  const createComment = async () => {
    if (text.trim() === "") {
      alert("Comment cannot be empty!");
      return;
    }
    try {
      const response = await fetch("http://localhost:5000/createcomment", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          content: text,
          username: JSON.parse(localStorage.getItem("user")).username,
          post_id: id,
        }),
      });
      const data = await response.json();
      if (response.ok) {
        fetchComments();
        makeText("");
        console.log("Comment added:", data);
      } else {
        console.error("Failed to create comment:", data.message);
        alert("Failed to create comment: " + data.message);
      }
    } catch (error) {
      console.error("Network error:", error);
      alert("Network error: Unable to connect to the server.");
    }
  };

  async function handleReport() {
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
      alert("You must be logged in to report.");
      return;
    }

    try {
      const response = await fetch("http://localhost:5000/reportpost", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          User_ID: user.user_id, // Assuming you store user_id in localStorage
          Post_ID: id,
        }),
      });
      const data = await response.json();
      if (response.ok) {
        setIsReported(true); // Update UI to reflect the reported status
        console.log("Post reported successfully:", data);
      } else {
        console.error("Failed to report post:", data.message);
        alert("Failed to report post: " + data.message);
      }
    } catch (error) {
      console.error("Network error while reporting post:", error);
      alert("Network error: Unable to connect to the server.");
    }
  }

  const handleLike = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
      alert("You must be logged in to like a post.");
      return;
    }

    try {
      const response = await fetch("http://localhost:5000/likepost", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          User_ID: user.user_id,
          Post_ID: id,
        }),
      });
      if (response.ok) {
        fetchVotes(); // Refresh votes count
      } else {
        console.error("Failed to like post:", response.statusText);
      }
    } catch (error) {
      console.error("Network error while liking post:", error);
    }
  };

  const handleDislike = async () => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
      alert("You must be logged in to dislike a post.");
      return;
    }

    try {
      const response = await fetch("http://localhost:5000/dislikepost", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          User_ID: user.user_id,
          Post_ID: id,
        }),
      });
      if (response.ok) {
        fetchVotes(); // Refresh votes count
      } else {
        console.error("Failed to dislike post:", response.statusText);
      }
    } catch (error) {
      console.error("Network error while disliking post:", error);
    }
  };

  const handleDelete = () => {
    deletePost(id);
  };

  return (
    <Card
      sx={{
        maxWidth: 345,
        width: "100%",
        borderRadius: 4,
        m: 2,
        overflow: "visible",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <CardContent>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          justifyContent={"space-between"}
        >
          {username}{" "}
          {isReported ? (
            <ReportProblemIcon color="error" />
          ) : (
            <Button
              style={{ marginLeft: "20px" }}
              variant="outlined"
              onClick={handleReport}
            >
              Report
            </Button>
          )}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {content}
        </Typography>
      </CardContent>
      <CardActions>
        <TextField
          label="Write comment"
          variant="outlined"
          size="small"
          fullWidth
          value={text}
          onChange={(e) => makeText(e.target.value)}
        />
        <Button size="small" onClick={createComment}>
          Submit
        </Button>
      </CardActions>
      <List
        dense
        sx={{ width: "100%", bgcolor: "background.paper", overflow: "auto" }}
      >
        {comments.map((comment, index) => (
          <ListItem key={index} divider>
            <ListItemText primary={comment} />
          </ListItem>
        ))}
      </List>
      <div>
        <Button onClick={handleLike}>
          <ThumbUpIcon />
          <Typography padding={1} variant="body2">
            {likesNum} Likes
          </Typography>
        </Button>
        <Button onClick={handleDislike}>
          <ThumbDownIcon />
          <Typography padding={1} variant="body2">
            {dislikesNum} Dislikes
          </Typography>
        </Button>
      </div>

      {isAdmin ? (
        <Button onClick={handleDelete}>
          <DeleteIcon color="error" />
        </Button>
      ) : null}
    </Card>
  );
}
