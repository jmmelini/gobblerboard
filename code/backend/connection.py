from flask import Flask, render_template, request, redirect, url_for, jsonify
import mysql.connector
from flask_cors import CORS 
import bcrypt
app = Flask(__name__)

CORS(app)

config = {
    'host': 'localhost',
    'user': 'root',
    'password': 'CHANGEME',
    'database': 'Gobblerboard'
}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/signup', methods=['POST'])
def signup():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')

    if not username or not password:
        return jsonify({'message': 'Missing username or password'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    cursor.execute("SELECT Username FROM User WHERE Username = %s", (username,))
    user_exists = cursor.fetchone()

    if user_exists:
        cursor.close()
        conn.close()
        return jsonify({'message': 'Username already exists'}), 409

    # Hash the password before storing it
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

    # Insert new user into database with a default Mod_Level of 0
    cursor.execute(
        "INSERT INTO User (Username, Password, Mod_Level) VALUES (%s, %s, %s)",
        (username, hashed_password, 0)
    )
    conn.commit()

    # Retrieve the user_id of the newly created user
    cursor.execute("SELECT User_ID FROM User WHERE Username = %s", (username,))
    user_id = cursor.fetchone()[0]  # Fetch the user_id from the query result

    cursor.close()
    conn.close()

    return jsonify({'message': 'User created successfully', 'user_id': user_id, 'username': username}), 201


@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')

    if not username or not password:
        return jsonify({'message': 'Missing username or password'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    cursor.execute("SELECT User_ID, Password FROM User WHERE Username = %s", (username,))
    user_data = cursor.fetchone()

    if user_data is None:
        cursor.close()
        conn.close()
        return jsonify({'message': 'Invalid username'}), 404

    user_id, stored_password = user_data
    if bcrypt.checkpw(password.encode('utf-8'), stored_password.encode('utf-8')):
        cursor.close()
        conn.close()
        return jsonify({'message': 'Login successful', 'user_id': user_id, 'username': username}), 200
    else:
        cursor.close()
        conn.close()
        return jsonify({'message': 'Invalid password'}), 401
    
@app.route('/posts', methods=['GET'])
def get_posts():
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor(dictionary=True) 

    query = """
    SELECT p.Post_ID, p.Content, u.Username
    FROM post p
    JOIN user u ON p.User_ID = u.User_ID
    """
    cursor.execute(query)
    posts = cursor.fetchall() 

    cursor.close()
    conn.close()

    return jsonify(posts)

@app.route('/createpost', methods=['POST'])
def create_post():
    data = request.get_json()
    user_id = data.get('User_ID')
    content = data.get('Content')

    if not user_id or not content:
        return jsonify({'message': 'Missing User_ID or content'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        cursor.execute(
            "INSERT INTO post (User_ID, Content) VALUES (%s, %s)",
            (user_id, content)
        )
        conn.commit()
        post_id = cursor.lastrowid
        
        cursor.execute(
            "SELECT Post_ID, User_ID, Content FROM post WHERE Post_ID = %s",
            (post_id,)
        )
        new_post = cursor.fetchone()
        new_post_data = {
            'Post_ID': new_post[0],
            'User_ID': new_post[1],
            'Content': new_post[2]
        }
    except mysql.connector.Error as err:
        print("Error: ", err)
        return jsonify({'message': 'Failed to create post'}), 500
    finally:
        cursor.close()
        conn.close()

    return jsonify(new_post_data), 201

@app.route('/deletepost/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        # Start transaction
        conn.start_transaction()

        # Delete votes associated with the post
        cursor.execute("DELETE FROM vote WHERE Post_ID = %s", (post_id,))
        
        # Delete reports associated with the post or its comments
        cursor.execute("DELETE FROM report WHERE Post_ID = %s OR Comment_ID IN (SELECT Comment_ID FROM comment WHERE Post_ID = %s)", (post_id, post_id))
        
        # Delete comments associated with the post
        cursor.execute("DELETE FROM comment WHERE Post_ID = %s", (post_id,))

        # Delete the post itself
        cursor.execute("DELETE FROM post WHERE Post_ID = %s", (post_id,))

        # Commit transaction
        conn.commit()

        if cursor.rowcount == 0:
            return jsonify({'message': 'No post was deleted'}), 404
        return jsonify({'message': 'Post and all related items deleted successfully'}), 200

    except mysql.connector.Error as err:
        # Rollback in case of error
        conn.rollback()
        print("Error: ", err)
        return jsonify({'message': 'Failed to delete post and related items'}), 500
    finally:
        cursor.close()
        conn.close()



@app.route('/createcomment', methods=['POST'])
def create_comment():
    data = request.get_json()
    content = data.get('content')
    username = data.get('username')
    post_id = data.get('post_id')

    if not content or not username or not post_id:
        return jsonify({'message': 'Missing data. Content, username, and post_id are required.'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    try:
        cursor.execute("SELECT User_ID FROM user WHERE Username = %s", (username,))
        user_result = cursor.fetchone()
        if not user_result:
            return jsonify({'message': 'User not found'}), 404
        user_id = user_result[0]

        cursor.execute(
            "INSERT INTO comment (Content, User_ID, Post_ID) VALUES (%s, %s, %s)",
            (content, user_id, post_id)
        )
        conn.commit()
        
        comment_id = cursor.lastrowid
        return jsonify({'message': 'Comment created successfully', 'Comment_ID': comment_id}), 201

    except mysql.connector.Error as err:
        print("Error: ", err)
        return jsonify({'message': 'Failed to create comment'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/comments', methods=['GET'])
def fetch_comments():
    post_id = request.args.get('post_id')

    if not post_id:
        return jsonify({'message': 'Post ID is required'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor(dictionary=True)  

    try:
        cursor.execute(
            "SELECT c.Comment_ID, c.Content, c.User_ID, c.Post_ID, u.Username FROM comment c JOIN user u ON c.User_ID = u.User_ID WHERE c.Post_ID = %s",
            (post_id,)
        )
        comments = cursor.fetchall()
        return jsonify(comments), 200
    except mysql.connector.Error as err:
        print("SQL Error: ", err)
        return jsonify({'message': 'Failed to fetch comments'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/getmodlevel/<int:user_id>', methods=['GET'])
def get_mod_level(user_id):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        cursor.execute("SELECT Mod_Level FROM user WHERE User_ID = %s", (user_id,))
        mod_level = cursor.fetchone()
        if mod_level:
            return jsonify({'modLevel': mod_level[0]}), 200
        else:
            return jsonify({'message': 'User not found'}), 404
    except mysql.connector.Error as err:
        return jsonify({'message': 'Database error', 'error': str(err)}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/checkreport/<int:post_id>', methods=['GET'])
def check_report(post_id):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        # Query to find any reports associated with the given post ID
        cursor.execute("SELECT * FROM report WHERE Post_ID = %s", (post_id,))
        report = cursor.fetchone()
        return jsonify({'reported': report is not None}), 200
    except mysql.connector.Error as err:
        print("SQL Error: ", err)
        return jsonify({'message': 'Failed to check for report'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/reportpost', methods=['POST'])
def report_post():
    data = request.get_json()
    user_id = data.get('User_ID')
    post_id = data.get('Post_ID')

    if not user_id or not post_id:
        return jsonify({'message': 'User ID and Post ID are required'}), 400

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        # Insert the report into the database
        cursor.execute(
            "INSERT INTO report (User_ID, Post_ID) VALUES (%s, %s)",
            (user_id, post_id)
        )
        conn.commit()
        return jsonify({'message': 'Report submitted successfully'}), 201
    except mysql.connector.Error as err:
        print("SQL Error: ", err)
        conn.rollback()
        return jsonify({'message': 'Failed to submit report'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/getvotes/<int:post_id>', methods=['GET'])
def get_votes(post_id):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        # Fetch likes
        cursor.execute("SELECT COUNT(*) FROM vote WHERE Post_ID = %s AND Vote_type = 0", (post_id,))
        likes = cursor.fetchone()[0]

        # Fetch dislikes
        cursor.execute("SELECT COUNT(*) FROM vote WHERE Post_ID = %s AND Vote_type = 1", (post_id,))
        dislikes = cursor.fetchone()[0]

        return jsonify({'likes': likes, 'dislikes': dislikes}), 200
    except mysql.connector.Error as err:
        print("SQL Error: ", err)
        return jsonify({'message': 'Failed to fetch votes'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/likepost', methods=['POST'])
def like_post():
    data = request.get_json()
    user_id = data['User_ID']
    post_id = data['Post_ID']
    vote_type = 0  # 0 for like

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        cursor.execute("""
            INSERT INTO vote (User_ID, Post_ID, Vote_type) VALUES (%s, %s, %s)
            ON DUPLICATE KEY UPDATE Vote_type = VALUES(Vote_type)
        """, (user_id, post_id, vote_type))
        conn.commit()
        return jsonify({'message': 'Like recorded'}), 200
    except mysql.connector.Error as err:
        conn.rollback()
        print("SQL Error: ", err)
        return jsonify({'message': 'Failed to record like'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/dislikepost', methods=['POST'])
def dislike_post():
    data = request.get_json()
    user_id = data['User_ID']
    post_id = data['Post_ID']
    vote_type = 1  # 1 for dislike

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    try:
        # Upsert vote
        cursor.execute("""
            INSERT INTO vote (User_ID, Post_ID, Vote_type) VALUES (%s, %s, %s)
            ON DUPLICATE KEY UPDATE Vote_type = VALUES(Vote_type)
        """, (user_id, post_id, vote_type))
        conn.commit()
        return jsonify({'message': 'Dislike recorded'}), 200
    except mysql.connector.Error as err:
        conn.rollback()
        print("SQL Error: ", err)
        return jsonify({'message': 'Failed to record dislike'}), 500
    finally:
        cursor.close()
        conn.close()


@app.route('/changePassword/<int:user_id>', methods=['POST'])
def change_password(user_id):
    data = request.get_json()
    old_password = data['oldPassword'].encode('utf-8')  # Ensure password is in bytes
    new_password = data['newPassword'].encode('utf-8')

    conn = mysql.connector.connect(**config)
    cursor = conn.cursor(buffered=True)
    
    try:
        cursor.execute("SELECT Password FROM user WHERE User_ID = %s", (user_id,))
        current_password_hash = cursor.fetchone()[0] 

        # Check if the old password is correct
        if bcrypt.checkpw(old_password, current_password_hash.encode('utf-8')):
            # If correct, hash the new password and update it
            new_password_hash = bcrypt.hashpw(new_password, bcrypt.gensalt())
            cursor.execute("UPDATE user SET Password = %s WHERE User_ID = %s", (new_password_hash, user_id))
            conn.commit()
            return jsonify({'message': 'Password updated successfully'}), 200
        else:
            return jsonify({'message': 'Old password is incorrect'}), 400
    except mysql.connector.Error as err:
        conn.rollback()
        print("SQL Error: ", err)
        return jsonify({'message': 'Database error'}), 500
    finally:
        cursor.close()
        conn.close()

@app.route('/reports', methods=['GET'])
def get_aggregate_data():
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    # A dictionary to hold the counts
    data = {}

    try:
        # Count total users
        cursor.execute("SELECT COUNT(*) FROM user")
        data['total_users'] = cursor.fetchone()[0]

        # Count total posts
        cursor.execute("SELECT COUNT(*) FROM post")
        data['total_posts'] = cursor.fetchone()[0]

        # Count total comments
        cursor.execute("SELECT COUNT(*) FROM comment")
        data['total_comments'] = cursor.fetchone()[0]

        # Count total reports
        cursor.execute("SELECT COUNT(*) FROM report")
        data['total_reports'] = cursor.fetchone()[0]

        # Count total likes
        cursor.execute("SELECT COUNT(*) FROM vote WHERE Vote_type = 0")
        data['total_likes'] = cursor.fetchone()[0]

        # Count total dislikes
        cursor.execute("SELECT COUNT(*) FROM vote WHERE Vote_type = 1")
        data['total_dislikes'] = cursor.fetchone()[0]

        return jsonify(data), 200
    except mysql.connector.Error as err:
        print("SQL Error: ", err)
        return jsonify({'message': 'Database error', 'error': str(err)}), 500
    finally:
        cursor.close()
        conn.close()



if __name__ == '__main__':
    app.run(debug=True)    
